# IOT Demo in Azure

# Create VM

#### Login
`az login`

#### Create group
`az group create --name demoGroup --location eastus`

#### Create vm
`az vm create --resource-group demoGroup --name demoVM --image UbuntuLTS --admin-username azureuse
r --generate-ssh-keys`

#### SSH into VM
`ssh azureuser@137.135.96.194`

#### Update apt-get available package
`sudo apt-get update`

#### Install Git
`sudo apt-get install git-core`

#### Install Docker
`sudo apt-get install apt-transport-https ca-certificates curl software-properties-common`

`curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`

`sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"`

`sudo apt-get update`

`sudo apt-get install docker-ce -y`

`sudo apt install docker-compose -y`

#### Verify Docker is installed (if you want)
`sudo docker run hello-world`

#### Pull down IOT code base
`sudo mkdir MFK_Hackathon_2018`

`cd MFK_Hackathon_2018`

`sudo git clone https://gitlab.com/MFK_Hackathon_2018/iot-platform-demo.git`

#### Build it
`cd iot-platform-demo`
`sudo docker-compose build`

#### Run it
`sudo docker-compose up -d`
- Note sometimes I had to run this twice to get the Grafana webserver to stay up

#### Open ports 3001 (Grafana) and 3002 (web server) in Azure for external access
- See the following, not perfect, but close enough for me.
https://blogs.msdn.microsoft.com/pkirchner/2016/02/02/allow-incoming-web-traffic-to-web-server-in-azure-vm/


