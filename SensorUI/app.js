const Influx = require('influx');
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const app = express();
const influx = new Influx.InfluxDB('http://grafana:grafana@influxdb:8086/sensordata');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(express.static(path.join(__dirname, '/public')));
app.set('port', 3002);

influx.getMeasurements()
    .then(names => console.log('My measurement names are: ' + names.join(', ')))
    .then(() => {
        app.listen(app.get('port'), () => {
            console.log(`Listening on ${app.get('port')}.`);
        });
    })
    .catch(error => console.log({error}));

app.get('/api/v1/get', (req, res) => {
    influx.query('SELECT * FROM sensordata order by time DESC limit 50')
        .then(result => {
            res.status(200).json(result);
        })
        .catch(error => res.status(500).text({error}));
});
