let latestTimeStamp = 0;

let refreshRate = 1000;
let last50;

let pollingTimer;

function pollForData() {
    timeout(1000, fetch('/api/v1/get'))
        .then(response => {
            if (response.status !== 200) {
                console.log(response);
            }
            return response;
        })
        .then(response => response.json())
        .then(parsedResponse => {
            if (parsedResponse.length > 0) {
                last50 = parsedResponse;

                if ($('#feed-link').parent().hasClass('active')) {
                    updateFeedTable(parsedResponse);
                }

                latestTimeStamp = parsedResponse[0].time;
                pollingTimer = setTimeout(pollForData, refreshRate);
            }
        })
        .catch(error => {
            console.log(error);
            pollingTimer = setTimeout(pollForData, refreshRate);
        });
}

function timeout(ms, promise) {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            reject(new Error("timeout"));
        }, ms);
        promise.then(resolve, reject);
    });
}

function lgRowBuilder(row) {
    let rowString = '<tr>'
        + '<td>' + new Date(row.time).toLocaleString() + '</td>'
        + '<td>' + row.id + '</td>'
        + '<td>' + row.data_type + '</td>'
        + '<td>' + row.model + '</td>';
    if (row.latitude == null) {
        rowString += '<td></td>';
    } else {
        rowString += '<td>' + parseFloat(row.latitude.toFixed(4)) + '</td>';
    }
    if (row.longitude == null) {
        rowString += '<td></td>';
    } else {
        rowString += '<td>' + parseFloat(row.longitude.toFixed(4)) + '</td>';
    }
    rowString += '</tr>';
    return rowString;
}

function smRowBuilder(row) {
    let rowString = '<tr>' +
        '<td>' + new Date(row.time).toLocaleString() + '</td>'
        + '<td>' + row.id + '</td>';
    if (row.latitude == null) {
        rowString += '<td></td>';
    } else {
        rowString += '<td>' + parseFloat(row.latitude.toFixed(4)) + '</td>';
    }
    if (row.longitude == null) {
        rowString += '<td></td>';
    } else {
        rowString += '<td>' + parseFloat(row.longitude.toFixed(4)) + '</td>';
    }
    rowString += '</tr>';
    return rowString;
}

function updateFeedTable(parsedResponse) {
    if ($(document).width() > 545) {
        $('#feed-table-lg > tbody').empty();
        for (let i = 0; i < 50 && i < parsedResponse.length; i++) {
            $('#feed-table-lg > tbody:last-child')
                .append(lgRowBuilder(parsedResponse[i]));
        }
    } else {
        $('#feed-table-sm > tbody').empty();
        for (let i = 0; i < 50 && i < parsedResponse.length; i++) {
            $('#feed-table-sm > tbody:last-child')
                .append(smRowBuilder(parsedResponse[i]));
        }
    }
}

function removeActive() {
    $('#nav-list').find('li').removeClass("active");
}

$(document).ready(function () {
    $('#feed-link').on('click', function (evt) {
        removeActive();
        $('#feed-container').css('display', 'block');
        $('#grafana-container').css('display', 'none');
        $('#feed-link').parent().addClass("active");
    });

    $('#slow-sin').on('click', function (evt) {
        removeActive();
        $('#feed-container').css('display', 'none');
        $('#slow-sin').parent().addClass('active');
        $('#grafana-container').css('display', 'inline-block');
        $('#grafana-iframe').attr("src",
            "http://" + window.location.hostname + ":3001/d-solo/5A_kFb2ik/mock-probe-dashboard?refresh=5s&orgId=1&panelId=3");
    });

    $('#fast-sin').on('click', function (evt) {
        removeActive();
        $('#feed-container').css('display', 'none');
        $('#fast-sin').parent().addClass('active');
        $('#grafana-container').css('display', 'inline-block');
        $('#grafana-iframe').attr("src",
            "http://" + window.location.hostname + ":3001/d-solo/5A_kFb2ik/mock-probe-dashboard?panelId=2&orgId=1");
    });

    $('#map').on('click', function (evt) {
        removeActive();
        $('#feed-container').css('display', 'none');
        $('#map').parent().addClass('active');
        $('#grafana-container').css('display', 'inline-block');
        $('#grafana-iframe').attr("src",
            "http://" + window.location.hostname + ":3001/d-solo/5A_kFb2ik/mock-probe-dashboard?panelId=9&orgId=1");
    });

    $('#vibration-crest').on('click', function (evt) {
        removeActive();
        $('#feed-container').css('display', 'none');
        $('#vibration-crest').parent().addClass('active');
        $('#grafana-container').css('display', 'inline-block');
        $('#grafana-iframe').attr("src",
            "http://" + window.location.hostname + ":3001/d-solo/xgYnO1Tiz/compressor?refresh=5s&orgId=1&panelId=1");
    });

    $('#temperature').on('click', function (evt) {
        removeActive();
        $('#feed-container').css('display', 'none');
        $('#temperature').parent().addClass('active');
        $('#grafana-container').css('display', 'inline-block');
        $('#grafana-iframe').attr("src",
            "http://" + window.location.hostname + ":3001/d-solo/xgYnO1Tiz/compressor?refresh=5s&orgId=1&panelId=2");
    });

    $('#velocity').on('click', function (evt) {
        removeActive();
        $('#feed-container').css('display', 'none');
        $('#velocity').parent().addClass('active');
        $('#grafana-container').css('display', 'inline-block');
        $('#grafana-iframe').attr("src",
            "http://" + window.location.hostname + ":3001/d-solo/xgYnO1Tiz/compressor?refresh=5s&orgId=1&panelId=3");
    });

    $('#vibration').on('click', function (evt) {
        removeActive();
        $('#feed-container').css('display', 'none');
        $('#vibration').parent().addClass('active');
        $('#grafana-container').css('display', 'inline-block');
        $('#grafana-iframe').attr("src",
            "http://" + window.location.hostname + ":3001/d-solo/xgYnO1Tiz/compressor?refresh=5s&orgId=1&panelId=4");
    });

    $('#pressure').on('click', function (evt) {
        removeActive();
        $('#feed-container').css('display', 'none');
        $('#pressure').parent().addClass('active');
        $('#grafana-container').css('display', 'inline-block');
        $('#grafana-iframe').attr("src",
            "http://" + window.location.hostname + ":3001/d-solo/xgYnO1Tiz/compressor?refresh=5s&orgId=1&panelId=5");
    });

    $('.nav-item').on('click', function(evt) {
        $('#navbarSupportedContent').removeClass('collapsed');
        $('#navbarSupportedContent').attr('aria-expanded', 'false');
        $('#navbarSupportedContent').removeClass('show');
    });
});

pollForData();
