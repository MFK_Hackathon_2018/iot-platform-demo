const DockDataCreator = require('./dockDataCreator');

const data = new DockDataCreator();

var host = "kafka:9092"

var kafka = require('kafka-node'),
    Producer = kafka.Producer,
    client = new kafka.KafkaClient({kafkaHost: host})
    producer = new Producer(client);
    
var payloads = [
    { topic: 'sensorDataJson', messages: data.messages }
];

producer.on('ready', () => {
    console.log("Ready");
    producer.send(payloads, (err, data) => {
        console.log(data);
    })
});

producer.on('error', function (err) {
    console.error("Error sending kafka payload " + err);
})