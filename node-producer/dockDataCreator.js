'use strict';

var moment = require('moment');
var _ = require('lodash');

const dockFrequenciesInMinutes = {
    1: 360,
    2: 480
};

const DockDataCreator = function () {
    const self = this;

    self.messages = [];

    var startDate = moment('2018-09-01');
    var endDate = moment('2018-09-15');

    for (var dock = 1; dock < 3; dock++) {
        console.log(startDate.toString());
        var dockFrequency = dockFrequenciesInMinutes[dock];
        var dateIdx = moment(startDate).set('time', '00:00');

        var idIdx = _.random(0, 100000);
        var nextSwitchDateTime = moment(dateIdx).add(_.random(60, dockFrequency), 'm');
        var isActive = false;

        while (dateIdx.isBefore(endDate)) {
            var i = dateIdx.get('h');

            if (dateIdx.isSameOrAfter(nextSwitchDateTime)) {
                var switchInMinutes = _.random(60, dockFrequency);
                nextSwitchDateTime = moment(dateIdx).add(switchInMinutes, 'm');
                isActive = !isActive;
            }

            self.messages.push(JSON.stringify(createMessage(idIdx, dateIdx, dock, isActive)));

            idIdx++;
            dateIdx.add(15, 'm');
        }

        console.log("Length after dock " + dock + ": " + self.messages.length);
    }
};

function createMessage(id, timestamp, dockNumber, isActive) {
    // console.log("Id: " + id + " " + timestamp + " dock " + dockNumber + " is in use " + isActive);

    var messageTemplate = {
        data_type: 'sensor',
        model: 'dock-simulation',
        id: id,
        source: 'dock ' + dockNumber,
        timestamp: timestamp.valueOf(),
        fields: {
            'isInUse': isActive ? 1 : 0
        }
    };

    return messageTemplate;
}

module.exports = DockDataCreator;