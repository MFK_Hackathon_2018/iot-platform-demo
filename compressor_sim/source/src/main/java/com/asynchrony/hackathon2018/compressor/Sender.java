package com.asynchrony.hackathon2018.compressor;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.concurrent.TimeUnit;

public class Sender {
    private final KafkaProducer producer;
    private final String topic;

    public Sender(KafkaProducer producer, String topic) {
        this.producer = producer;
        this.topic = topic;
    }

    public void send(String message) {
        final ProducerRecord<Long, String> record = new ProducerRecord<>(topic, message);
        try {
            producer.send(record).get(30, TimeUnit.SECONDS);
            System.out.printf("Sent: %s\n", message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
