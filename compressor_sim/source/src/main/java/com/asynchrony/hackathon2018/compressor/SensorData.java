package com.asynchrony.hackathon2018.compressor;

import java.util.HashMap;
import java.util.Map;

public class SensorData {
    private String data_type;
    private String model;
    private String id;
    private String source;
    private long timestamp;
    private Map<String, Object> fields = new HashMap<>();

    public SensorData(String data_type, String model, String id, String source, long timestamp, Map<String, Object> fields) {
        this.data_type = data_type;
        this.model = model;
        this.id = id;
        this.source = source;
        this.timestamp = timestamp;
        this.fields = fields;
    }

    public SensorData() {

    }

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public Map<String, Object> getFields() {
        return fields;
    }

    public void setFields(Map<String, Object> fields) {
        this.fields = fields;
    }
}
