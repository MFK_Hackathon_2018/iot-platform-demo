package com.asynchrony.hackathon2018.compressor;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import org.apache.commons.lang.StringUtils;
import org.apache.kafka.clients.producer.KafkaProducer;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class App {
    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        OptionParser parser = new OptionParser();
        OptionSpec<String> hostOption = parser.accepts("h", "host").withRequiredArg().ofType(String.class);
        OptionSpec<Integer> portOption = parser.accepts("p", "port").withRequiredArg().ofType(Integer.class);
        OptionSpec<String> topicOption = parser.accepts("t", "topic").withRequiredArg().ofType(String.class).defaultsTo("sensorDataJson");
        OptionSpec<Long> rateOption = parser.accepts("i", "reporting interval in milliseconds").withRequiredArg().ofType(Long.class).defaultsTo(100L);
        OptionSet options = parser.parse(args);

        String host = options.valueOf(hostOption);
        String topic = options.valueOf(topicOption);
        int port = options.valueOf(portOption);
        long interval = options.valueOf(rateOption);
        if (StringUtils.isBlank(host) || port == 0 || StringUtils.isBlank(topic)) {
            System.err.println("Missing required argument(s).");
            parser.printHelpOn(System.err);
        }

        try (InputStreamReader reader = new InputStreamReader(App.class.getResourceAsStream("/export.json"))) {
            TypeToken<List<SensorData>> typeToken = new TypeToken<List<SensorData>>() {};
            List<SensorData> sensorData = new Gson().fromJson(reader, typeToken.getType());

            KafkaProducer producer = new ProducerFactory().create(host, port);
            Sender sender = new Sender(producer, topic);
            Reporter reporter = new Reporter(sender,
                    sensorData);
            reporter.start(interval).get();
        }
    }
}
