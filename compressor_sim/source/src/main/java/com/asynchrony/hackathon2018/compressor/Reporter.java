package com.asynchrony.hackathon2018.compressor;

import com.google.gson.Gson;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class Reporter {
    private final Gson gson = new Gson();
    private final Sender sender;
    private List<SensorData> sensorDataList;

    public Reporter(Sender sender, List<SensorData> sensorDataList) {
        this.sender = sender;
        this.sensorDataList = sensorDataList;
    }

    public Future<?> start(long interval) {
        final AtomicInteger index = new AtomicInteger(0);

        return Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(() -> {
            SensorData sensorData = sensorDataList.get(index.get());

            sensorData.setSource(getSource());
            sensorData.setTimestamp(System.currentTimeMillis());

            String message = gson.toJson(sensorData);
            sender.send(message);

            index.set((index.get() + 1) % sensorDataList.size());
        }, 0L, interval, TimeUnit.MILLISECONDS);
    }

    private String getSource() {
        try(final DatagramSocket socket = new DatagramSocket()){
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            return socket.getLocalAddress().getHostAddress();
        } catch (Exception e) {
            return "unknown";
        }
    }
}
