package com.asynchrony.hackathon2018.probe;

import com.google.gson.Gson;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class Reporter {
    private final Gson gson = new Gson();
    private final Sender sender;

    public Reporter(Sender sender) {
        this.sender = sender;
    }

    public Future<?> start(long interval) {
        String id = String.format("%05d", RandomUtils.nextInt() % 100000);
        long calcOffset = RandomUtils.nextLong() % 20000L;

        return Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(() -> {
            long now = System.currentTimeMillis();

            Map<String, Object> sensorMessage = new HashMap();
            sensorMessage.put("data_type", "sensor");
            sensorMessage.put("model", "Mock Probe");
            sensorMessage.put("id", id);
            sensorMessage.put("source", getSource());
            sensorMessage.put("timestamp", now);

            Map<String, Object> fields = new HashMap();
            fields.put("slow_sine", Math.sin((now + calcOffset) / 20000.0));
            fields.put("fast_sine", Math.sin((now + calcOffset) / 3000.0));
            fields.put("random_percent", RandomUtils.nextInt(101));
            fields.put("random_boolean", RandomUtils.nextBoolean());
            fields.put("random_string", RandomStringUtils.randomAlphabetic(5));
            fields.put("latitude", 38.623393 + (Math.sin((now + calcOffset)) / 1000.0));
            fields.put("longitude", -90.195749 + (Math.cos((now + calcOffset)) / 1000.0));

            sensorMessage.put("fields", fields);

            String message = gson.toJson(sensorMessage);
            sender.send(message);
        }, 0L, interval, TimeUnit.MILLISECONDS);
    }

    private String getSource() {
        try(final DatagramSocket socket = new DatagramSocket()){
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            return socket.getLocalAddress().getHostAddress();
        } catch (Exception e) {
            return "unknown";
        }
    }
}
