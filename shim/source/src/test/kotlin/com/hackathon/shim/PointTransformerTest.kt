package com.hackathon.shim

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import org.assertj.core.api.Assertions.assertThat
import org.influxdb.dto.Point
import org.junit.Test
import java.util.concurrent.TimeUnit

class PointTransformerTest {

    private val timeProvider = mock<TimeProvider>()

    private val pointTransformer = PointTransformer()

    @Test
    fun fromJson_withAllFields() {
        whenever(timeProvider.currentTimeMillis()).thenReturn(12345)
        val json = "{\"data_type\":\"sensor\",\"model\":\"Mock Probe\",\"id\":\"12345\",\"source\":\"172.17.0.6\",\"fields\":{\"random_string\":\"kapZu\",\"fast_sine\":0.999881476327551,\"random_boolean\":false,\"slow_sine\":-0.6476901732254396,\"random_percent\":46},\"timestamp\":1535833151071}\n"

        val expectedPoint = Point.measurement(PointTransformer.MEASUREMENT_SENSOR_DATA)
                .time(1535833151071, TimeUnit.MILLISECONDS)
                .tag("data_type", "sensor")
                .tag("model", "Mock Probe")
                .tag("id", "12345")
                .tag("source", "172.17.0.6")
                .addField("random_string", "kapZu")
                .addField("fast_sine", 0.999881476327551)
                .addField("random_boolean", false)
                .addField("slow_sine", -0.6476901732254396)
                .addField("random_percent", 46.0)
                .build()

        val actualPoint = pointTransformer.sensorDataPointFromJson(json)

        assertThat(actualPoint).isEqualToComparingFieldByField(expectedPoint)
    }
}