package com.hackathon.shim

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.influxdb.dto.Point
import org.junit.Test
import org.springframework.data.influxdb.InfluxDBTemplate
import java.util.concurrent.TimeUnit

class SensorDataServiceTest {

    private val pointTransformer = mock<PointTransformer>()
    private val influxDBTemplate = mock<InfluxDBTemplate<Point>>()

    private val exampleData: String = "{\"jsonField\":\"jsonValue\"}"

    private val sensorDataService: SensorDataService = SensorDataService(influxDBTemplate, pointTransformer)

    @Test
    fun listen_savesPointToDatabase() {

        val expectedPoint: Point = Point.measurement(PointTransformer.MEASUREMENT_SENSOR_DATA)
                .time(12345, TimeUnit.MILLISECONDS)
                .addField("jsonField", "jsonValue")
                .build()
        whenever(pointTransformer.sensorDataPointFromJson(exampleData)).thenReturn(expectedPoint)

        val consumerRecord: ConsumerRecord<Any, Any> = ConsumerRecord<Any, Any>(SensorDataService.TOPIC_SENSOR_DATA_JSON,
                1,
                0,
                null,
                exampleData)

        sensorDataService.listen(consumerRecord)

        verify(influxDBTemplate).write(expectedPoint)
    }

}