package com.hackathon.shim

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.influxdb.dto.Point
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.data.influxdb.InfluxDBTemplate
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
class ShimApplicationTest {

    private val influxDBTemplate: InfluxDBTemplate<Point> = mock()

    private val application = ShimApplication(influxDBTemplate)

    @Test
    fun createsContext() {

    }

    @Test
    fun run_createsDatabase() {
        application.run(null)

        verify(influxDBTemplate).createDatabase()
    }
}