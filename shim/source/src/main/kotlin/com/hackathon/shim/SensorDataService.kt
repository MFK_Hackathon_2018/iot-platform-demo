package com.hackathon.shim

import org.apache.kafka.clients.consumer.ConsumerRecord
import org.influxdb.dto.Point
import org.springframework.data.influxdb.InfluxDBTemplate
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Service

@Service
class SensorDataService(private val influxDBTemplate: InfluxDBTemplate<Point>,
                        private val pointTransformer: PointTransformer) {

    @KafkaListener(topics = [TOPIC_SENSOR_DATA_JSON])
    fun listen(consumerRecord: ConsumerRecord<Any, Any>) {
        val point = pointTransformer.sensorDataPointFromJson(consumerRecord.value().toString())
        influxDBTemplate.write(point)
    }

    companion object {
        const val TOPIC_SENSOR_DATA_JSON = "sensorDataJson"
    }
}