package com.hackathon.shim

import org.influxdb.dto.Point
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.influxdb.InfluxDBTemplate

@SpringBootApplication
class ShimApplication(private val influxDBTemplate: InfluxDBTemplate<Point>) : CommandLineRunner {

    override fun run(vararg args: String?) {
        influxDBTemplate.createDatabase()
    }

}

fun main(args: Array<String>) {
    runApplication<ShimApplication>(*args)
}
