package com.hackathon.shim

import org.springframework.stereotype.Component

@Component
class TimeProvider {

    fun currentTimeMillis() : Long {
        return System.currentTimeMillis()
    }
}