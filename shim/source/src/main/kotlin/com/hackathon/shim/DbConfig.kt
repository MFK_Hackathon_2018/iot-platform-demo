package com.hackathon.shim

import org.influxdb.dto.Point
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.influxdb.DefaultInfluxDBTemplate
import org.springframework.data.influxdb.InfluxDBConnectionFactory
import org.springframework.data.influxdb.InfluxDBProperties
import org.springframework.data.influxdb.InfluxDBTemplate
import org.springframework.data.influxdb.converter.PointConverter

@Configuration
@EnableConfigurationProperties(InfluxDBProperties::class)
class DbConfig {

    @Bean
    fun connectionFactory(properties: InfluxDBProperties): InfluxDBConnectionFactory {
        return InfluxDBConnectionFactory(properties)
    }

    @Bean
    fun influxDBTemplate(connectionFactory: InfluxDBConnectionFactory): InfluxDBTemplate<Point> {
        return InfluxDBTemplate(connectionFactory, PointConverter())
    }

    @Bean
    fun defaultTemplate(connectionFactory: InfluxDBConnectionFactory): DefaultInfluxDBTemplate {
        return DefaultInfluxDBTemplate(connectionFactory)
    }

}