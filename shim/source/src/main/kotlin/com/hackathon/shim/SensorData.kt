package com.hackathon.shim

data class SensorData(val data_type: String,
                      val model: String,
                      val id: String,
                      val source: String,
                      val timestamp: Long,
                      val fields: Map<String, Any>)