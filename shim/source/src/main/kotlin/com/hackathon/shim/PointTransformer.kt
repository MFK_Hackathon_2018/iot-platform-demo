package com.hackathon.shim

import com.google.gson.Gson
import org.influxdb.dto.Point
import org.springframework.stereotype.Component
import java.util.concurrent.TimeUnit

@Component
class PointTransformer {
    private val gson = Gson()

    fun sensorDataPointFromJson(json: String): Point {
        val data = gson.fromJson(json, SensorData::class.java)

        val point = Point.measurement(MEASUREMENT_SENSOR_DATA)
                .time(data.timestamp, TimeUnit.MILLISECONDS)
                .tag(TAG_DATA_TYPE, data.data_type)
                .tag(TAG_MODEL, data.model)
                .tag(TAG_ID, data.id)
                .tag(TAG_SOURCE, data.source)
                .fields(data.fields)

        return point.build()
    }

    companion object {
        const val MEASUREMENT_SENSOR_DATA = "sensordata"
        const val TAG_DATA_TYPE = "data_type"
        const val TAG_MODEL = "model"
        const val TAG_ID = "id"
        const val TAG_SOURCE = "source"
    }
}