# MFK Hackathon - IOT Platform Demo

## Overview

This repository is intended to include the Docker configurations and source for the entire MFK Hackathon project.

For containers that run a custom application, the Dockerfile is set up to build the source code during the container build. Take a look at the [Shim Dockerfile](shim/Dockerfile) for an example.

## Usage

From root directory:

```shell
docker-compose build
docker-compose up -d --scale probe=3
```

The ```--scale``` argument is used to specify the number of mock probes to start.

## Capabilities

Integrations so far are: Mock Probe → Kafka → Shim → InfluxDB → Grafana.

The Mock Probe publishes mock sensor data to topic "sensorDataJson" on the Kafka broker every second. It has been made a part of the default demo deployment to provide an automatic source of data for front-end developers to try using in Grafana.

The Shim subscribes to topic "sensorDataJson" on the Kafka broker, and writes data to InfluxDB database "sensordata" in measurement "sensordata":

```shell
root@10e09a3cbba5:/# influx
Connected to http://localhost:8086 version 1.5.3
InfluxDB shell version: 1.5.3
> use sensordata
Using database sensordata
> select * from sensordata
name: sensordata
time                data_type fast_sine             id    latitude           longitude          model      random_boolean random_percent random_string slow_sine             source
----                --------- ---------             --    --------           ---------          -----      -------------- -------------- ------------- ---------             ------
1536028145354000000 sensor    0.0363945267758014    22044 38.62271282182112  -90.19648204682318 Mock Probe false          54             GkAow         -0.8885142919980771   172.17.0.6
1536028145919000000 sensor    -0.5009204215281347   92890 38.62344076034715  -90.19674785882349 Mock Probe true           67             UdPJy         -0.9969048098362967   172.17.0.7
1536028146051000000 sensor    0.8157757716268781    28341 38.62360813472388  -90.19672558437966 Mock Probe true           84             ETngN         -0.8171487135324388   172.17.0.8

```

The Grafana dashboard will be available at http://localhost:3001/login, with credentials admin/admin. It currently shows strip charts and a PLI map for connected Mock Probes.

